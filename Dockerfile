
# This Dockerfile simulates the use
# on a new machine.
# - in this case an ubuntu container

FROM ubuntu:22.04

# install ansible and dependencies
RUN apt-get update
RUN apt-get install -y \
    openssh-client \
    python3-pip \
    git

RUN pip3 install ansible

# execute ansible playbook

# docker should not cache the below
ARG CACHEBUST=1

# ENV SSH_AUTH_SOCK /tmp/auth.sock

RUN mkdir -p -m 0700 ~/.ssh && ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
RUN --mount=type=ssh ssh -Tvv git@gitlab.com

# NOTE: ssh-agent is forwarded during build
RUN --mount=type=ssh \
    ansible-pull \
    --url https://gitlab.com/lentilus-dotfiles/dev-ready.git \
    -i $(hostname --short), \
    playbook.yaml -vv

