#!/bin/bash

# execute this script in this directory
# otherwise the Dockerfile wont be detected
# optionally provide a tag as the first argument

if [[ -z $1 ]]; then
    tag="dev-box"
else
    tag=$1
fi

# WARNING: ssh-agent is forwarded during build
export DOCKER_BUILDKIT=1
docker build -t "$tag" --build-arg CACHEBUST="$(date +%s)" --ssh default="$SSH_AUTH_SOCK" .

